package data;

public class FloydWarshallAlgorithm {
    private static final int MATRIX_SIZE = 4;
    private int[][] matrixArray;

    public FloydWarshallAlgorithm() {
        matrixArray = getMatriArray();
    }

    public void doFloydWarshall() {
        for (int k = 0; k < MATRIX_SIZE; k++) {
            for (int i = 0; i < MATRIX_SIZE; i++) {
                for (int j = 0; j < MATRIX_SIZE; j++) {
                    matrixArray[i][j] = Math.min(matrixArray[i][j], matrixArray[i][k] + matrixArray[k][j]);
                }
            }
        }
    }

    public String getResult() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < MATRIX_SIZE; i++) {
            for (int j = 0; j < MATRIX_SIZE; j++) {
                builder.append(String.valueOf(matrixArray[i][j]) + " ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    private int[][] getMatriArray() {
        return new int[][]{
                new int[]{0, 10, 1, 101},
                new int[]{10, 0, 101, 5},
                new int[]{1, 101, 0, 2},
                new int[]{101, 5, 2, 0}
        };
    }
}
