package view.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;

import com.graphtheory.R;

import data.FloydWarshallAlgorithm;
import view.MainListener;
import view.viewholder.ViewHolder;

public class MainActivity extends Activity implements MainListener {

    private BackgroundTask backgroundTask;
    private ViewHolder viewHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindView();
        backgroundTask = new BackgroundTask();
    }

    private void bindView() {
        setContentView(R.layout.activity_main);
        viewHolder = new ViewHolder(MainActivity.this);
        viewHolder.bindView();
    }

    @Override
    public void findGraphMinimumPath() {
        if (backgroundTask.getStatus()!= AsyncTask.Status.RUNNING) {
            backgroundTask = new BackgroundTask();
            backgroundTask.execute();
        }
    }

    @Override
    public Activity getCurrentActivity() {
        return MainActivity.this;
    }

    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private FloydWarshallAlgorithm algorithm = new FloydWarshallAlgorithm();

        @Override
        protected Void doInBackground(Void... voids) {
            algorithm.doFloydWarshall();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            viewHolder.getTxtResult().setText(algorithm.getResult());
        }
    }
}
