package view.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.graphtheory.R;

import lombok.Getter;
import view.MainListener;

public class ViewHolder {
    @Getter
    private Button btnFindMinPath;
    @Getter
    private TextView txtResult;
    private MainListener listener;

    public ViewHolder(MainListener listener) {
        this.listener = listener;
        btnFindMinPath = (Button) listener.getCurrentActivity().findViewById(R.id.btn_find_path);
        txtResult = (TextView) listener.getCurrentActivity().findViewById(R.id.txt_result);
    }

    public void bindView() {
        btnFindMinPath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.findGraphMinimumPath();
            }
        });
    }
}