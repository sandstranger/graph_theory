package view;

import android.app.Activity;

public interface MainListener {
    void findGraphMinimumPath();

    Activity getCurrentActivity();
}
